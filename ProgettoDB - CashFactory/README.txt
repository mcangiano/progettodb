Per testare il database seguire i seguenti step:
-Aprire pgAdmin 
-Creare un Database
-Aprire "Query Tool" sul database appena creato
-Copiare, incollare e eseguire i file presenti nella cartella 'SQL' in quest'ordine:
	-Create tables.txt
	-Triggers.txt (possono essere inseriti anche alla fine, ma in questo modo siamo sicuri che
	le operazioni di insert dei valori di esempio siano consistenti)
	-Insert Values.txt
	-Query Sql.txt (per testarne il funzionamento)
---------------------------------
è stato utilizzato:
pgAdmin 4.19
